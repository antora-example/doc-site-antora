# Projeto inicial de Antora Documentation

Este projeto tem como objetivo entender o funcionamento do Antora Documentation para aplicar com a escrita de BDD.

Este repositório é a "main" de todos os outros repositórios que estaremos coletando a documentação, nesse sentido todos os BDDs que foram criados em [documentation-a](https://gitlab.com/antora-example/documentation-a) serão buildadas por este repositório.

Para visualizar a pagina buildada basta digitar https://bdd-teste.surge.sh/ no navegador

- Para execução do antora para coleta dos textos do outro repositório basta executar o comando `npx antora --fetch antora-playbook.yml`